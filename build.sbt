ThisBuild / version := "1.0.0-SNAPSHOT"

// Java 1.8 is required to run SBT!
// Java 1.8 is also required when building / running this project.
// If you use IntelliJ, ctrl + alt + s , search sbt, and select java 1.8 as sdk
// Same for project configuration : Maj + Ctrl + Alt + S : project : sdk : java 1.8
// Failing to do so will result in critical binding errors.
ThisBuild / organization := "fr.epsi.rennes.pokemon.scala"
ThisBuild / crossJavaVersions := Seq("1.8")
ThisBuild / scalaVersion := "2.12.19"
ThisBuild / sbtVersion := "1.9.9"

lazy val root = (project in file("."))
  .settings(
    name := "scala-pokemon"
  )


val sparkVersion = "3.5.1"
val breezeVersion = "2.1.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,

)
// other dependencies here
libraryDependencies += "org.scalanlp" %% "breeze" % breezeVersion
libraryDependencies += "org.scalanlp" %% "breeze-natives" % breezeVersion
libraryDependencies += "org.scalanlp" %% "breeze-viz" % breezeVersion

resolvers ++= Seq(
  // other resolvers here
  // if you want to use snapshot builds (currently 0.12-SNAPSHOT), use this.
  "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
)