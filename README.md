# Spark Scala

## Sujet

Equilibrage d'un jeu Pokemon

#### Données

- Dataset 1

| id | Name                 | Type1 | Type2  | Total | Generation | Legendary |
|----|----------------------|-------|--------|-------|------------|-----------|
| 1  | Bulbasaur            | Grass | Poison | 318   | 1          | false     |
| 2  | Ivysaur              | Grass | Poison | 405   | 1          | false     |
| 3  | Venusaur             | Grass | Poison | 525   | 1          | false     |
| 3  | VenusaurMega Venusaur| Grass | Poison | 625   | 1          | false     |
| 4  | Charmander           | Fire  | NULL   | 309   | 1          | false     |
| 5  | Charmeleon           | Fire  | NULL   | 405   | 1          | false     |
| 6  | Charizard            | Fire  | Flying | 534   | 1          | false     |
| 6  | CharizardMega Charizard X | Fire | Dragon | 634   | 1    | false     |
| 6  | CharizardMega Charizard Y | Fire | Flying | 634   | 1    | false     |
| 7  | Squirtle             | Water | NULL   | 314   | 1          | false     |
| 8  | Wartortle            | Water | NULL   | 405   | 1          | false     |
| 9  | Blastoise            | Water | NULL   | 530   | 1          | false     |
| 9  | BlastoiseMega Blastoise  | Water | NULL | 630   | 1        | false     |
| 10 | Caterpie             | Bug   | NULL   | 195   | 1          | false     |
| 11 | Metapod              | Bug   | NULL   | 205   | 1          | false     |
| 12 | Butterfree           | Bug   | Flying | 395   | 1          | false     |
| 13 | Weedle               | Bug   | Poison | 195   | 1          | false     |
| 14 | Kakuna               | Bug   | Poison | 205   | 1          | false     |
| 15 | Beedrill             | Bug   | Poison | 395   | 1          | false     |
| 15 | BeedrillMega Beedrill| Bug   | Poison | 495   | 1          | false     |

- Dataset 2

| id | Name                 | Total |  HP | Attack | Defense | AttackSpecial | DefenseSpecial | Speed |
|----|----------------------|-------|-----|--------|---------|---------------|----------------|-------|
| 1  | Bulbasaur            | 318   | 45  | 49     | 49      | 65            | 65             | 45    |
| 2  | Ivysaur              | 405   | 60  | 62     | 63      | 80            | 80             | 60    |
| 3  | Venusaur             | 525   | 80  | 82     | 83      | 100           | 100            | 80    |
| 3  | VenusaurMega Venusaur| 625   | 80  | 100    | 123     | 122           | 120            | 80    |
| 4  | Charmander           | 309   | 39  | 52     | 43      | 60            | 50             | 65    |
| 5  | Charmeleon           | 405   | 58  | 64     | 58      | 80            | 65             | 80    |
| 6  | Charizard            | 534   | 78  | 84     | 78      | 109           | 85             | 100   |
| 6  | CharizardMega Charizard X | 634 | 78  | 130    | 111     | 130           | 85             | 100   |
| 6  | CharizardMega Charizard Y | 634 | 78  | 104    | 78      | 159           | 115            | 100   |
| 7  | Squirtle             | 314   | 44  | 48     | 65      | 50            | 64             | 43    |
| 8  | Wartortle            | 405   | 59  | 63     | 80      | 65            | 80             | 58    |
| 9  | Blastoise            | 530   | 79  | 83     | 100     | 85            | 105            | 78    |
| 9  | BlastoiseMega Blastoise  | 630 | 79  | 103    | 120     | 135           | 115            | 78    |
| 10 | Caterpie             | 195   | 45  | 30     | 35      | 20            | 20             | 45    |
| 11 | Metapod              | 205   | 50  | 20     | 55      | 25            | 25             | 30    |
| 12 | Butterfree           | 395   | 60  | 45     | 50      | 90            | 80             | 70    |
| 13 | Weedle               | 195   | 40  | 35     | 30      | 20            | 20             | 50    |
| 14 | Kakuna               | 205   | 45  | 25     | 50      | 25            | 25             | 35    |
| 15 | Beedrill             | 395   | 65  | 90     | 40      | 45            | 80             | 75    |
| 15 | BeedrillMega Beedrill| 495   | 65  | 150    | 40      | 15            | 80             | 145   |

#### Objectifs

- [X] Charger

Charger la donnée brute

- [X] Traiter (/7)

Transformer (conversion type str>int, int<>bool, plusieurs col en tableau)

Nettoyer : (qualité, missing, outliers)

- [X] Analyser (/7)

Pourquoi ya des pb d'equilibrage dans le jeu pokemon

Aggregats de données

Tri (pokemon les plus forts), sous dossier par type de pokemon

Tendances (feu overall > air, legendaires etc)

Au moins 2 aggregations (sum avg std)

- [X] Visualiser (/6)

Visualisation pertinente des données après transformation
