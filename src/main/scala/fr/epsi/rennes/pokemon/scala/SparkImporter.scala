package fr.epsi.rennes.pokemon.scala

import fr.epsi.rennes.pokemon.scala.Main.getSparkSession
import org.apache.spark.sql.{DataFrame, SparkSession}

class SparkImporter {

  val spark: SparkSession = getSparkSession()

  def importCsv(file: String): DataFrame = {
    spark
      .read
      .option("header", "true")  // Use first line of all files as header
      .option("inferSchema", "true") // Automatically infer data types
      .csv(file)
  }

}
