package fr.epsi.rennes.pokemon.scala

import breeze.plot._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

object Main extends SparkRunner {
  private var show = false

  def main(args: Array[String]): Unit = {
    // Allow to pass an argument to show the dataframes
    if (args.length > 0) {
      if (args.contains {
        "--show"
      }) {
        show = true
      }
    }
    // Load the data and create the view "pokemons"
    val pokemons = load_pokemons()

    // Make additional views (for sql easy access)
    make_stats_views(pokemons)

    // Prepare the data for visualisation
    prepare_count_pokemons_by_types()
    prepare_avg_stats_by_types()
    prepare_avg_stats_by_generations()

    // Create the visualisations
    val plotter = new SparkVisuals(show)
    plotter.plot_from_view("count_by_type", "Type", "count", "Pokemons by type", _type = "bar", savePath = "target/pokemons_by_type.png")
    plotter.plot_from_view("avg_stats_by_type", "Type", "avg_Total", "Average stats by type", _type = "bar", savePath = "target/avg_stats_by_type.png")
    plotter.plot_from_view("avg_stats_by_generation", "Generation", "avg_Total", "Average stats by generation", _type = "bar", savePath = "target/avg_stats_by_generation.png")

    // working area
    val df = pokemons
    val aggregationResult_stats = df.groupBy("Type1")
      .agg(
        count("Name").alias("Count"),
        avg("HP").alias("AverageHP"),
        avg("Attack").alias("AverageAttack"),
        avg("Defense").alias("AverageDefense"),
        min("HP").alias("MinHP"),
        min("Attack").alias("MinAttack"),
        min("Defense").alias("MinDefense"),
        max("HP").alias("MaxHP"),
        max("Attack").alias("MaxAttack"),
        max("Defense").alias("MaxDefense")
      )
      .orderBy("Type1")
    aggregationResult_stats.show()

    val types = aggregationResult_stats.select("Type1").collect.map(_.getString(0))
    val avgHP = aggregationResult_stats.select("AverageHP").collect.map(_.getDouble(0))
    val avgAttack = aggregationResult_stats.select("AverageAttack").collect.map(_.getDouble(0))
    val avgDefense = aggregationResult_stats.select("AverageDefense").collect.map(_.getDouble(0))

    // Line graph 1
    val f = Figure(name = "Average Stats by Type")
    val p = f.subplot(0)

    p += plot(types.indices.map(_.toDouble), avgHP, name = "Average HP")
    p += plot(types.indices.map(_.toDouble), avgAttack, name = "Average Attack")
    p += plot(types.indices.map(_.toDouble), avgDefense, name = "Average Defense")

    p.xlabel = "Types"
    p.ylabel = "Average Stats"
    p.title = "Average Stats by Type"
    p.legend = true

    f.refresh()
    f.saveas("target/line_graph1.png")
    // Line graph 2
    import breeze.linalg.DenseVector
    val f2 = Figure(name = "Line Plot of Average HP, Average Attack, and Average Defense by Type")
    val p2 = f2.subplot(0)

    types.indices.foreach { i =>
      val xValues = DenseVector(0.0, 1.0, 2.0)
      val yValues = DenseVector(avgHP(i), avgAttack(i), avgDefense(i))
      p2 += plot(xValues, yValues, '-', name = types(i))
    }

    p2.title = "Line Plot of Average HP, Average Attack, and Average Defense by Type"
    p2.xlabel = "Attribute"
    p2.ylabel = "Average Value"
    // p2.xticks(0 to 2, Seq("Average HP", "Average Attack", "Average Defense")) todo
    p2.legend = true

    f2.refresh()
    f2.saveas("target/line_graph2.png")

    // Close the spark session on signal
    done()
  }

  private def prepare_avg_stats_by_types(): Unit = {
    val avg_stats_by_type = make_view("avg_stats_by_type", // Use know view to count Type1 or Type2 == Type
      """
        |SELECT Type, ROUND(AVG(HP), 2) as avg_HP, ROUND(AVG(Attack), 2) as avg_Attack, Round(AVG(Defense), 2) as avg_Defense,
        |       Round(AVG(AttackSpecial), 2) as avg_SpAtk, Round(AVG(DefenseSpecial), 2) as avg_SpDef, Round(AVG(Speed), 2) as avg_Speed,
        |       Round(AVG(HP + Attack + Defense + AttackSpecial + DefenseSpecial + Speed), 2) as avg_Total
        |FROM unique_types
        |JOIN pokemons ON (
        |   pokemons.Type1 = unique_types.Type
        |   OR pokemons.Type2 = unique_types.Type
        | )
        |GROUP BY Type
        |ORDER BY avg_Total DESC
        |""".stripMargin, show)

    System.out.println("Average stats by type : 'avg_stats_by_type'")
    System.out.println("TOP 5 :")
    avg_stats_by_type.show(5)
  }

  private def prepare_avg_stats_by_generations(): Unit = {
    val avg_stats_by_generation = make_view("avg_stats_by_generation", // Use know view to count Type1 or Type2 == Type
      """
        |SELECT Generation, ROUND(AVG(HP), 2) as avg_HP, ROUND(AVG(Attack), 2) as avg_Attack, Round(AVG(Defense), 2) as avg_Defense,
        |       Round(AVG(AttackSpecial), 2) as avg_SpAtk, Round(AVG(DefenseSpecial), 2) as avg_SpDef, Round(AVG(Speed), 2) as avg_Speed,
        |       Round(AVG(HP + Attack + Defense + AttackSpecial + DefenseSpecial + Speed), 2) as avg_Total
        |FROM pokemons
        |GROUP BY Generation
        |ORDER BY avg_Total DESC
        |""".stripMargin, show)

    System.out.println("Average stats by generation : 'avg_stats_by_generation'")
    System.out.println("TOP 5 :")
    avg_stats_by_generation.show(5)
  }

  private def prepare_count_pokemons_by_types(): DataFrame = {
    val count_by_type = make_view("count_by_type",
      //       Use know view to count Type1 or Type2 == Type
      """
        |SELECT Type, COUNT(*) as count
        |FROM unique_types
        |JOIN pokemons ON (
        |   pokemons.Type1 = unique_types.Type
        |   OR pokemons.Type2 = unique_types.Type
        | )
        |GROUP BY Type
        |ORDER BY count DESC
        |""".stripMargin,
      show)

    val sum_of_all = count_by_type.select("count").collect()
      .map(_.getLong(0)).sum
    System.out.println("Sum of pokemons by type : %d".format(sum_of_all))
    count_by_type
  }

  private def make_stats_views(df:DataFrame): Unit = {
    System.out.println("\n\nLegendary pokemons : 'legendary_pokemons'")
    val legendary_pokemons = df.filter(df("Legendary") === "True")
    legendary_pokemons.createOrReplaceTempView("legendary_pokemons")


    System.out.println("\n\nNon legendary pokemons : 'non_legendary_pokemons'")
    val non_legendary_pokemons = df.filter(df("Legendary") === "False")
    non_legendary_pokemons.createOrReplaceTempView("non_legendary_pokemons")


    System.out.println("\n\nUnique types of pokemons : 'unique_types'")
    val unique_types = df.select("Type1").union(df.select("Type2")).na.drop.distinct().toDF("Type")
    unique_types.createOrReplaceTempView("unique_types")
    unique_types.persist() // used multiple times
    System.out.println("Unique types of pokemons : %d".format(unique_types.count()))


    System.out.println("\n\nUnique generations of pokemons : 'unique_generations'")
    val unique_generations = df.select("Generation").distinct()
    unique_generations.createOrReplaceTempView("unique_generations")
    System.out.println("Unique generations of pokemons : %d".format(unique_generations.count()))


    System.out.println("\n\nTop 20 pokemons by total stats : 'top_20_pokemons'")
    val top_20_pokemons = df.orderBy(df("Total").desc).limit(20)
    top_20_pokemons.createOrReplaceTempView("top_20_pokemons")
    top_20_pokemons.show(20)
  }


  private def load_pokemons(): DataFrame = {
    System.out.println("Importing data from csv files")
    val importer = new SparkImporter()

    System.out.println("Importing data from infos")
    val infos = importer.importCsv("src/main/resources/csv/pokemonInfo.csv")
    val df_infos = infos.toDF()
    if (show) {
      infos.show() // Show the data before
      System.out.println("Infos after preliminary cleaning")
      df_infos.show(5)
    }

    System.out.println("Importing data from stats")
    val stats = importer.importCsv("src/main/resources/csv/pokemonStats.csv")
      // Both tables have id, Total in common (similar)
      .drop("id")
      .drop("Total")
    if (show) {
      stats.show() // Show the data before
      System.out.println("Stats after preliminary cleaning")
    }

    System.out.println("Joining dataframes on Name, without duplicate columns")
    val pokemons = df_infos.join(stats, "Name", "fullouter")
    pokemons.createOrReplaceTempView("pokemons")
    if (show) {
      System.out.println("Created view 'pokemons' with the following data:")
      pokemons.show(5)
    }
    System.out.println("Import done, persisting data for future use (decreases performances on small datasets)")
    pokemons.persist() // Cache data in memory and persist to disk for faster access on future calls
    pokemons
  }
}