package fr.epsi.rennes.pokemon.scala

import breeze.plot._
import fr.epsi.rennes.pokemon.scala.Main.getSparkSession
import org.apache.spark.sql.DataFrame

// import Breeze visualisation

/**
 * This class is used to plot graphs from a DataFrame
 * <br>
 * It is using the Breeze library to plot the graphs.
 * gets the SparkSession from the Main class
 * Use example :<br>
 * <pre>
 * val plotter = new SparkVisuals()
 * plotter.plot_from_view("count_by_type", col1="Type", col2="count",
 * title="Pokemons by type", _type="bar",
 * savePath="target/pokemons_by_type.png")
 * plotter.plot_from_view("count_by_type", "Type", y="count", title="Pokemons by type", _type="line")
 * </pre> */
class SparkVisuals {
  private val spark = getSparkSession()

  private var current_plot_style = "line"
  private var title: String = ""
  private var f: Figure = null
  private var p: Plot = null
  var show = false

  /**
   * Create a new plotter
   *
   * @param show if true, show the DataFrame used for the plot before plotting */
  def this(show: Boolean = false) {
    this()
    this.show = show
  }

  /**
   * Plot a graph from a DataFrame
   *
   * @param view : the name of the view to use (spark.sql("SELECT * FROM %s".format(view)).toDF())
   * @param col1 : the name of the column to use as x
   * @param col2 : the name of the column to use as y
   * @param title : the title of the graph
   * @param xLabel : the label of the x axis (default to col1)
   * @param yLabel : the label of the y axis (default to col2)
   * @param _type: the type of the plot (default to line)<br>
   *               'type_' possible values :<br>
   *               - line : line plot<br>
   *               - bar : bar plot<br>
   * @param savePath : save the plot as a .png file
   */
  def plot_from_view(view: String, col1: String, col2: String, title: String, xLabel: String = null, yLabel: String = null, _type: String = "line", savePath: String = null): Unit = {
    set_plot_sytle(_type, title)
    val df = spark.sql("SELECT * FROM %s".format(view)).toDF()
    if (show) {
      System.out.println("Showing DataFrame used for plot : %s -> %s".format(view, title))
      df.show()
    }
    val x = getNumercialColumn(df, col1)
    val y = getNumercialColumn(df, col2)

    System.out.println("Plotting %s graph with %d points".format(_type, x.length))
    trace_plot_style(x, y)
    set_labels(col1, col2, title, xLabel, yLabel)

    save_or_show(savePath)
  }

  /**
   * Save current plot as a .png file
   *
   * @param savePath : the path to save the file */
  private def save_or_show(savePath: String): Unit = {
    if (savePath != null) {
      f.visible = true // for exercise
      f.saveas(savePath)
    } else {
      f.visible = true
    }
  }

  /**
   * Set the labels for the axis and the title of the plot
   *
   * @param col1   : the name of the column to use as x
   * @param col2   : the name of the column to use as y
   * @param title  : the title of the graph
   * @param xLabel : the label of the x axis (default to col1)
   * @param yLabel : the label of the y axis (default to col2) */
  private def set_labels(col1: String, col2: String, title: String, xLabel: String, yLabel: String) = {
    p.xlabel = if (xLabel == null) col1.capitalize else xLabel
    p.ylabel = if (yLabel == null) col2.capitalize else yLabel
    p.title = title
  }

  /**
   * Set the plot style and the title for the current plot
   *
   * @param _type : the type of the plot (default to line)
   * @param title : the title of the graph */
  private def set_plot_sytle(_type: String, title: String) = {
    setType(_type)
    this.title = title
  }

  /**
   * Trace the plot with the current style
   *
   * @param x     : the x values
   * @param y     : the y values
   * @param _type : the type of the plot (default to line)
   * @return the figure */
  private def trace_plot_style(x: Array[Long], y: Array[Long]) = {
    f = Figure(name = title)
    p = f.subplot(0)
    current_plot_style match {
      case "bar" => bar_plot(x, y)
      case _ => {
        if (current_plot_style != "line") System.out.println("Unknown type : %s, using default : line".format(current_plot_style))
        breeze.plot.plot(x, y, name = title)
      }
    }
  }

  private def setType(_type: String) = {
    this.current_plot_style = _type match {
      case "line" => "line"
      case "bar" => "bar"
      case _ => {
        System.out.println("Unknown type : %s, using default : line".format(_type))
        "line"
      }
    }
  }

  private def bar_plot(x: Array[Long], y: Array[Long]) = {
    // trace one line from 0 to y for each x -> bar plot
    for (i <- x.indices) yield { // for each x, p=plot((x, x), (0, y))
      val _x = Array(x(i), x(i))
      val _y = Array(0, y(i))
      p += breeze.plot.plot(_x, _y, name = title)
    }
  }

  private def getNumercialColumn(data: DataFrame, colName: String): Array[Long] = {
    System.out.println("Getting numerical column " + colName)
    System.out.println("Type : " + data.schema(colName).dataType.typeName)
    if (data.schema(colName).dataType.typeName == "string") { // If the column is a string, we can use getString
      // return the index of the value in the unique values
      getIndexedNumericalFromString(data, colName)
    } else if (data.schema(colName).dataType.typeName == "double") { // If the column is double, we can use getDouble
      data.select(colName).collect().map(_.getDouble(0).toLong)
    } else if (data.schema(colName).dataType.typeName == "float") { // If the column is float, we can use getFloat
      data.select(colName).collect().map(_.getFloat(0).toLong)
    } else if (data.schema(colName).dataType.typeName == "integer") { // If the column is integer, we can use getInt
      data.select(colName).collect().map(_.getInt(0).toLong)
    } else { // If the column is numeric, we can use getLong
      data.select(colName).collect().map(_.getLong(0))
    }
  }

  private def getIndexedNumericalFromString(data: DataFrame, colName: String): Array[Long] = {
    val uq = getUniqueValuesFromColName(data, colName)
    if (show) {
      System.out.println("Unique values for %s : %s".format(colName, uq.mkString(", ")))
    }
    data.select(colName).collect().map(_.getString(0)).map(t => {
      uq.indexOf(t).toLong + 1
    })
  }

  private def getUniqueValuesFromColName(data: DataFrame, colName: String): Array[String] = {
    data.select(colName).distinct().collect().map(_.getString(0)) // Get unique values
  }

}
