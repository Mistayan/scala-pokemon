package fr.epsi.rennes.pokemon.scala

import org.apache.spark.sql.{DataFrame, SparkSession}


/**
 * This class is used to run the spark session and create views
 * to access the data easily. <br>
 * Use example :<br>
 * <pre>
 * class Main extends SparkRunner {
 * def main(args: Array[String]): Unit = {
 * // import data in the session
 * val importer = new SparkImporter()
 * val pokemons = importer.importCsv("src/main/resources/pokemon.csv")
 * pokemons.createOrReplaceTempView("pokemons")
 *
 * // interact with spark
 * val sql_result = spark.sql("SELECT * FROM pokemons")
 * sql_result.show()
 * val new_view = make_view("new_view_grassy",
 * "SELECT * FROM pokemons WHERE (Type1 = 'Grass' OR Type2 = 'Grass')", show = true)
 * }
 * }
 * </pre>
 *
 */
class SparkRunner {
  // built on instanciation.
  // The spark session is the main entry point for all spark operations
  val spark = SparkSession
    .builder
    .master("local[*]")
    .appName("rendu-group")
    .config("spark.sql.warehouse.dir", "./target/spark-warehouse")
    .config("spark.driver.memory", "2g")
    .config("spark.executor.memory", "2g")
    .getOrCreate()

  // set driver log level to ERROR
  spark.sparkContext.setLogLevel("ERROR")

  // CLASS INIT DONE

  /**
   * Get the spark session
   *
   * @return the spark session
   */
  def getSparkSession(): SparkSession = {
    spark
  }

  /**
   * Make a view for the selected query
   *
   * @param name  the name of the view
   * @param query the query to use
   * @param show  if true, show the dataframe resulting from the query (the new view)
   * @return the SQL dataframe resulting from the query
   */
  def make_view(name:String, query: String, show: Boolean=false): DataFrame = {
    val df = spark.sql(query)
    df.createOrReplaceTempView(name)
    if (show) {
      df.show()
    }
    df
  }

  /**
   * Await signal to close the spark session
   */
  def done(): Unit = {
    println("Press any key to leave")
    scala.io.StdIn.readLine()

    spark.stop()
  }
}